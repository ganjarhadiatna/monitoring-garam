<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return redirect('login');
})->name('welcome');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'salt'], function () {
    // ui
    Route::get('', 'SaltController@index')->name('ui-salt');
    Route::get('/create', 'SaltController@create')->name('ui-salt-create');
    Route::get('/monitoring/{id}', 'SaltController@monitoring')->name('ui-salt-monitoring');
    Route::get('/logsms/{id}', 'SaltController@logsms')->name('ui-salt-logsms');
    Route::get('/edit/{id}', 'SaltController@edit')->name('ui-salt-edit');

    // crud
    Route::post('/save', 'SaltController@save')->name('ui-salt-save');
    Route::post('/update', 'SaltController@update')->name('ui-salt-update');
    Route::post('/delete', 'SaltController@delete')->name('ui-salt-delete');
});



Route::group(['prefix' => 'monitoring'], function () {
    // ui
    Route::get('', 'HistoryController@index')->name('ui-monitoring');
    Route::get('/create', 'HistoryController@create')->name('ui-monitoring-create');
    Route::get('/edit/{id}', 'HistoryController@edit')->name('ui-monitoring-edit');

    // crud
    Route::post('/save', 'HistoryController@save')->name('ui-monitoring-save');
    Route::post('/update', 'HistoryController@update')->name('ui-monitoring-update');
    Route::post('/delete', 'HistoryController@delete')->name('ui-monitoring-delete');
    Route::post('/delete-by-salt-id', 'HistoryController@deleteBySaltsId')->name('ui-monitoring-delete-by-salt-id');
});


Route::group(['prefix' => 'logsms'], function () {
    // ui
    Route::get('', 'LogsmsController@index')->name('ui-logsms');
    Route::get('/create', 'LogsmsController@create')->name('ui-logsms-create');
    Route::get('/edit/{id}', 'LogsmsController@edit')->name('ui-logsms-edit');

    // crud
    Route::post('/save', 'LogsmsController@save')->name('ui-logsms-save');
    Route::post('/update', 'LogsmsController@update')->name('ui-logsms-update');
    Route::post('/delete', 'LogsmsController@delete')->name('ui-logsms-delete');
    Route::post('/delete-by-salt-id', 'LogsmsController@deleteBySaltsId')->name('ui-logsms-delete-by-salt-id');
});