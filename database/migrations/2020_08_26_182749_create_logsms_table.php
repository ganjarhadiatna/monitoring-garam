<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logsms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('status', ['0', '1'])->default('0');
            $table->unsignedBigInteger('salts_id');
            $table->timestamps();

            $table->foreign('salts_id')->references('id')->on('salts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logsms');
    }
}
