<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaltsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 4)->unique();
            $table->string('name');
            $table->enum('status', ['0', '1'])->default('0');
            $table->unsignedBigInteger('users_id');
            $table->timestamps();

            // activites
            $table->foreign('users_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salts');
    }
}
