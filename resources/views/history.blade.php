@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Hasil Monitoring Kadar Garam</div>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Kode</th>
                            <th scope="col">Alat</th>
                            <th scope="col">PH</th>
                            <th scope="col">TDS</th>
                            <th scope="col">Tanggal</th>
                            <th scope="col" width="100">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @foreach($history as $dt)
                            <tr>
                                <th scope="row">{{ $i }}</th>
                                <td>{{ $dt->code }}</td>
                                <td>{{ $dt->name }}</td>
                                <td>{{ $dt->ph }}</td>
                                <td>{{ $dt->tds }}</td>
                                <td>{{ $dt->created_at }}</td>
                                <td scope="row">
                                    <button class="btn btn-danger" type="button">Hapus</button>
                                </td>
                            </tr>
                            <?php $i++; ?>
                        @endforeach
                    </tbody>
                </table>
                {{ $history->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
