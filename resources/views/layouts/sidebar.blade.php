<nav id="sidebar">
    <div class="sidebar-header">
        <h3>{{ 'Monitoring Air Tua' }}</h3>
    </div>

    <ul class="list-unstyled components">
        <li>
            <a href="{{ route('home') }}">Dashboard</a>
        </li>
        <li>
            <a href="{{ route('ui-salt') }}">Daftar Alat</a>
        </li>
        <li>
            <a href="{{ route('ui-monitoring') }}">Data Monitoring</a>
        </li>
        <li>
            <a href="{{ route('ui-logsms') }}">Data Log SMS</a>
        </li>
    </ul>
</nav>