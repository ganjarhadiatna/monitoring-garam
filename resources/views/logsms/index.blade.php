@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        <h3 style="margin-top: 6px;">{{ $title }}</h3>
                    </div>
                    <div class="ml-auto">
                    @if($buttonClearAll)
                        <button class="btn btn-danger" data-toggle="modal" data-target="#deleteAllModal{{ $garamId }}">
                            <i class="fa fa-lw fa-trash-alt"></i>
                        </button>

                        <div class="modal fade" id="deleteAllModal{{ $garamId }}" tabindex="-1" role="dialog" aria-labelledby="deleteAllModalLabel{{ $garamId }}" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="deleteAllModalLabel{{ $garamId }}">Peringatan</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Semua data log sms akan dihapus secara permanen, lanjutkan?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>

                                    <a class="btn btn-danger" 
                                        href="{{ route('ui-logsms-delete-by-salt-id') }}"
                                        onclick="event.preventDefault(); document.getElementById('id-form-{{ $garamId }}').submit();"
                                        >
                                        Lanjutkan
                                    </a>

                                    <form id="id-form-{{ $garamId }}" action="{{ route('ui-logsms-delete-by-salt-id') }}" method="POST" style="display: block;">
                                        @csrf
                                        <input type="hidden" name="salt_id" value="{{ $garamId }}" />
                                    </form>
                                </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Kode</th>
                                <th scope="col">Alat</th>
                                <th scope="col">Status SMS</th>
                                <th scope="col">Tanggal Dibuat</th>
                                @if($buttonClearAll)<th width="70"></th>@endif
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($data as $dt)
                                <tr>
                                    <th scope="row">{{ $i }}</th>
                                    <td>{{ $dt->code }}</td>
                                    <td>{{ $dt->name }}</td>
                                    <td>{{ $dt->status == 1 ? 'SMS Terkirim' : 'SMS Tidak Terkirim' }}</td>
                                    <td>{{ $dt->created_at }}</td>
                                    @if($buttonClearAll)
                                    <td>
                                        <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $dt->id }}">
                                            <i class="fa fa-lw fa-trash-alt"></i>
                                        </button>

                                        <div class="modal fade" id="deleteModal{{ $dt->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel{{ $dt->id }}" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="deleteModalLabel{{ $dt->id }}">Peringatan</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Data log sms akan dihapus secara permanen, lanjutkan?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>

                                                    <a class="btn btn-danger" 
                                                        href="{{ route('ui-logsms-delete') }}"
                                                        onclick="event.preventDefault(); document.getElementById('id-form-{{ $dt->id }}').submit();"
                                                        >
                                                        Lanjutkan
                                                    </a>

                                                    <form id="id-form-{{ $dt->id }}" action="{{ route('ui-logsms-delete') }}" method="POST" style="display: block;">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{ $dt->id }}" />
                                                    </form>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    @endif
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
