@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h3 style="margin-top: 6px;">Dashboard</h3>
                </div>

                <div class="card-body">
                    @foreach($data as $dt)
                    <div style="padding-top: 15px; padding-bottom: 15px;">
                        <div>
                            <h4>{{ $dt->name }}</h4>
                            <p>{{ $dt->code }}</p>
                        </div>
                        <div class="wrapper">
                            <div style="width: 100%; margin-right: 15px;">
                                <div class="card">
                                    <div class="card-body wrapper">
                                        <div class="mr-auto">
                                            <i class="fa fa-4x fa-chart-line" style="color: #33a5da;"></i>
                                        </div>
                                        <div class="ml-auto" style="text-align: right;">
                                            <h4>{{ $dt->ph ? $dt->ph : '0' }}</h4>
                                            <p>PH</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="width: 100%; margin-right: 15px;">
                                <div class="card">
                                    <div class="card-body wrapper">
                                        <div class="mr-auto">
                                            <i class="fa fa-4x fa-chart-line" style="color: #33a5da;"></i>
                                        </div>
                                        <div class="ml-auto" style="text-align: right;">
                                            <h4>{{ $dt->tds ? $dt->tds : '0' }}</h4>
                                            <p>BAUME</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="width: 100%; margin-right: 15px;">
                                <div class="card">
                                    <div class="card-body wrapper">
                                        <div class="mr-auto">
                                            <i class="fa fa-4x fa-battery-full" style="color: #33a5da;"></i>
                                        </div>
                                        <div class="ml-auto" style="text-align: right;">
                                            <h4>{{ $dt->accu ? round((($dt->accu / 13.2) * 100), 2) : '0' }}%</h4>
                                            <p>Status Baterai</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="width: 100%;">
                                <div class="card">
                                    <div class="card-body wrapper">
                                        <div class="mr-auto">
                                            <i class="fa fa-4x fa-bolt" style="color: #33a5da;"></i>
                                        </div>
                                        <div class="ml-auto" style="text-align: right;">
                                            <h4>{{ $dt->panel ? $dt->panel : '0' }}</h4>
                                            <p>Status SP{{ $dt->panel > 15 ? ' (Sedang Mengisi)' : ' (Tidak Mengisi)' }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="margin-top: 15px;">
                            @if($dt->tds >= 9 && $dt->tds <= 24)
                                <div class="alert alert-primary" role="alert" id="alert-success">
                                    Air Sudah Memenuhi Standar
                                </div>
                            @else
                                <div class="alert alert-warning" role="alert" id="alert-danger">
                                    Air Belum Memenuhi Standar
                                </div>
                            @endif
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            
        </div>
    </div>
</div>


@endsection
