@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h3 style="margin-top: 6px;">Dashboard</h3>
                </div>

                <div class="card-body">
                    <div style="width: 100%">
                        {!! $phChart->container() !!}
                    </div>
                    <div style="width: 100%">
                        {!! $baumeChart->container() !!}
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>

{{-- ChartScript --}}
@if($phChart)
    {!! $phChart->script() !!}
@endif

@if($baumeChart)
    {!! $baumeChart->script() !!}
@endif

@endsection
