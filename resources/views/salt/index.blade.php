@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        <h3 style="margin-top: 6px;">Daftar Alat</h3>
                    </div>
                    <div class="ml-auto">
                        <a href="{{ route('ui-salt-create') }}" class="btn btn-primary">
                            <i class="fa fa-lw fa-plus"></i>
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Kode</th>
                                <th scope="col">Alat</th>
                                <th scope="col">PH</th>
                                <th scope="col">Baume</th>
                                <th scope="col">Status Baterai</th>
                                <th scope="col">Status SP</th>
                                <th scope="col">Keterangan</th>
                                <th scope="col">Admin</th>
                                <th scope="col">Tanggal Dibuat</th>
                                <th width="210"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($data as $dt)
                                <tr>
                                    <th scope="row">{{ $i }}</th>
                                    <td>{{ $dt->code }}</td>
                                    <td>{{ $dt->name }}</td>
                                    <td id="data-ph-{{$dt->id}}">{{ $dt->ph }}</td>
                                    <td id="data-tds-{{$dt->id}}">{{ $dt->tds }}</td>
                                    <td id="data-accu-{{$dt->id}}">{{ $dt->accu ? round((($dt->accu / 13.2) * 100), 2) : '0' }}%</td>
                                    <td id="data-panel-{{$dt->id}}">{{ $dt->panel }}{{ $dt->panel > 15 ? ' (Sedang Mengisi)' : ' (Tidak Mengisi)' }}</td>
                                    <td id="data-status-{{$dt->id}}">{{ ($dt->tds >= 9 && $dt->tds <= 24) ? 'Air Sudah Memenuhi Standar' : 'Air Belum Memenuhi Standar' }}</td>
                                    <td>{{ $dt->user_name }}</td>
                                    <td>{{ $dt->created_at }}</td>
                                    <td>
                                        <a href="{{ route('ui-salt-logsms', $dt->id) }}" class="btn btn-primary">
                                            <i class="fa fa-lw fa-envelope"></i>
                                        </a>
                                        <a href="{{ route('ui-salt-monitoring', $dt->id) }}" class="btn btn-primary">
                                            <i class="fa fa-lw fa-chart-bar"></i>
                                        </a>
                                        <a href="{{ route('ui-salt-edit', $dt->id) }}" class="btn btn-warning">
                                            <i class="fa fa-lw fa-pencil-alt"></i>
                                        </a>
                                        <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $dt->id }}">
                                            <i class="fa fa-lw fa-trash-alt"></i>
                                        </button>
                                        <div class="modal fade" id="deleteModal{{ $dt->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel{{ $dt->id }}" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="deleteModalLabel{{ $dt->id }}">Peringatan</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Data akan dihapus secara permanen, lanjutkan?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>

                                                    <a class="btn btn-danger" 
                                                        href="{{ route('ui-salt-delete') }}"
                                                        onclick="event.preventDefault(); document.getElementById('id-form-{{ $dt->id }}').submit();"
                                                        >
                                                        Lanjutkan
                                                    </a>

                                                    <form id="id-form-{{ $dt->id }}" action="{{ route('ui-salt-delete') }}" method="POST" style="display: block;">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{ $dt->id }}" />
                                                    </form>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div>
            </div>
            
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">Peringatan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Divisi akan dihapus secara permanen, lanjutkan?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-primary">Lanjutkan</button>
      </div>
    </div>
  </div>
</div>

<script>
    function getMonitoring () {
        $.ajax({
            url: "{{ url('api/history/get') }}",
            dataType: 'json'
        }).done(function(data) {
            console.log(data);
            var dt = data;
            for (let index = 0; index < dt.length; index++) {
                const element = dt[index];
                $('#data-ph-' + element.id).text(element.ph ? element.ph : '0');
                $('#data-tds-' + element.id).text(element.tds ? element.tds : '0');
                $('#data-accu-' + element.id).text((element.accu ? ((element.accu / 13.2) * 100).toFixed(2) : '0') + '%');
                $('#data-panel-' + element.id).text((element.panel ? element.panel : '0') + (element.panel > 15 ? ' (Sedang Mengisi)' : ' (Tidak Mengisi)'));
                $('#data-status-' + element.id).text((element.tds >= 9 && element.tds <= 24) ? 'Air Sudah Memenuhi Standar' : 'Air Belum Memenuhi Standar' );
            }
        });
    }

    $(document).ready(function () {
        setInterval(() => {
            getMonitoring();
        }, 2000);
    });
</script>

@endsection
