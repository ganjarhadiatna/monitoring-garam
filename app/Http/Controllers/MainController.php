<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\History;
use App\Salt;
use App\Logsms;

class MainController extends Controller
{
    public function save($code, $ph, $tds, $voltageAccu, $voltagePanel)
    {
        // find salt id
        $salt = Salt::where(['code' => $code])->first();
        if ($salt) {
            $id = $salt->id;
        } else {
            $id = '0';
        }


        if ($tds >= 20) {
            Salt::where(['id' => $salt->id])->update(['status' => '1']);
        } else {
            Salt::where(['id' => $salt->id])->update(['status' => '0']);
        }
        
        $data = [
            'ph' => $ph,
            'tds' => $tds,
            'accu' => $voltageAccu,
            'panel' => $voltagePanel,
            'salts_id' => $id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];

        $service = History::insert($data);
        if ($service) {
            $out = "success";
        } else {
            $out = "error";
        }

        return $out;
    }

    public function logsms($code, $status)
    {
        // find salt id
        $salt = Salt::where(['code' => $code])->first();
        if ($salt) {
            $id = $salt->id;
        } else {
            $id = '0';
        }

        $data = [
            'status' => $status,
            'salts_id' => $id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];

        $service = Logsms::insert($data);
        if ($service) {
            $out = "success";
        } else {
            $out = "error";
        }

        return $out;
    }

    public function get()
    {
        $data = Salt::GetAllWithHistory();
        return response()->json($data, 200);
    }
}
