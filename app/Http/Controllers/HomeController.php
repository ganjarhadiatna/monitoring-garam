<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\History;
use App\Salt;
use App\Charts\PhChart;
use App\Charts\BaumeChart;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $fillColors = [
        //     "rgba(255, 99, 132, 0.2)",
        //     "rgba(22,160,133, 0.2)",
        //     "rgba(255, 205, 86, 0.2)",
        //     "rgba(51,105,232, 0.2)",
        //     "rgba(244,67,54, 0.2)",
        //     "rgba(34,198,246, 0.2)",
        //     "rgba(153, 102, 255, 0.2)",
        //     "rgba(255, 159, 64, 0.2)",
        //     "rgba(233,30,99, 0.2)",
        //     "rgba(205,220,57, 0.2)"

        // ];

        // $dataPh = [];
        // $dataBaume = [];
        // $tgl = Carbon::now()->day();
        // $tanggal = [];
        // $data = History::whereDay('created_at', '<=', $tgl)->limit(10)->get();
        // // echo $data;
        // foreach ($data as $dt) {
        //     array_push($dataPh, $dt->ph);
        //     array_push($dataBaume, $dt->tds);
        //     array_push($tanggal, date_format($dt->created_at, "d/m/y"));
        // }

        // $phChart = new PhChart;
        // $phChart->labels($tanggal);
        // $phChart->dataset('PH Monitoring', 'line', $dataPh)->backgroundcolor($fillColors);

        // $baumeChart = new BaumeChart;
        // $baumeChart->labels($tanggal);
        // $baumeChart->dataset('Baume Monitoring', 'line', $dataBaume)->backgroundcolor($fillColors);

        // return view('home', ['phChart' => $phChart, 'baumeChart' => $baumeChart]);

        $data = Salt::GetAll(10);

        return view('home', ['data' => $data]);
    }
}
