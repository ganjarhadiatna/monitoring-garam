<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\History;
use App\Salt;

class HistoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = History::GetAll(10);
        return view('history.index', ['title' => 'Data monitoring', 'buttonClearAll' => false, 'data' => $data]);
    }

    public function byCode($code)
    {
        $salt = Salt::where(['code' => $code])->first();
        if ($salt) 
        {
            $data = History::GetById($salt->id, 10);
        } 
        else 
        {
            $data = [];
        }
        return view('history.index', ['title' => 'Data monitoring per alat', 'buttonClearAll' => true, 'data' => $data]);
    }

    // api
    public function save($code, $ph, $tds)
    {
        // find salt id
        $salt = Salt::where(['code' => $code])->first();
        if ($salt) {
            $id = $salt->id;
        } else {
            $id = '0';
        }
        
        $data = [
            'ph' => $ph,
            'tds' => $tds,
            'salts_id' => $id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];

        $service = History::insert($data);
        if ($service) {
            $out = "success";
        } else {
            $out = "error";
        }

        // $code = $request->input('code');
        return $out;
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $data = History::where(['id' => $id])->first();
        $service = History::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/salt/monitoring/'. $data->salts_id);
        }
        else 
        {
            return redirect('/salt/monitoring/'. $data->salts_id);
        }
    }

    public function deleteBySaltsId(Request $request)
    {
        $this->validate($request, [
            'salt_id' => 'required',
        ]);

        $salt_id = $request->input('salt_id');

        $service = History::where(['salts_id' => $salt_id])->delete();

        if ($service) 
        {
            return redirect('/salt/monitoring/'. $salt_id);
        }
        else 
        {
            return redirect('/salt/monitoring/'. $salt_id);
        }
    }
}