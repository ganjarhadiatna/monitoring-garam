<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Logsms;
use App\Salt;

class LogsmsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Logsms::GetAll(10);
        return view('logsms.index', ['title' => 'Data log sms', 'buttonClearAll' => false, 'data' => $data]);
    }

    public function byCode($code)
    {
        $salt = Salt::where(['code' => $code])->first();
        if ($salt) 
        {
            $data = Logsms::GetById($salt->id, 10);
        } 
        else 
        {
            $data = [];
        }
        return view('logsms.index', ['title' => 'Data log sms per alat', 'buttonClearAll' => true, 'data' => $data]);
    }

    // api
    public function save($code, $note, $status)
    {
        // find salt id
        $salt = Salt::where(['code' => $code])->first();
        if ($salt) {
            $id = $salt->id;
        } else {
            $id = '0';
        }

        $data = [
            'note' => $note,
            'status' => $status,
            'salts_id' => $id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];

        $service = Logsms::insert($data);
        if ($service) {
            $out = "success";
        } else {
            $out = "error";
        }

        // $code = $request->input('code');
        return $out;
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $data = Logsms::where(['id' => $id])->first();
        $service = Logsms::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/salt/logsms/'. $data->salts_id);
        }
        else 
        {
            return redirect('/salt/logsms/'. $data->salts_id);
        }
    }

    public function deleteBySaltsId(Request $request)
    {
        $this->validate($request, [
            'salt_id' => 'required',
        ]);

        $salt_id = $request->input('salt_id');

        $service = Logsms::where(['salts_id' => $salt_id])->delete();

        if ($service) 
        {
            return redirect('/salt/logsms/'. $salt_id);
        }
        else 
        {
            return redirect('/salt/logsms/'. $salt_id);
        }
    }
}
