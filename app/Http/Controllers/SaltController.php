<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Salt;
use App\History;
use App\Logsms;
use Auth;

class SaltController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Salt::GetAll(5);
        return view('salt.index', ['data' => $data]);
    }

    public function monitoring($id)
    {
        $data = History::GetById($id, 10);
        return view('history.index', ['title' => 'Data monitoring per alat', 'buttonClearAll' => true, 'garamId' => $id, 'data' => $data]);
    }

    public function logsms($id)
    {
        $data = Logsms::GetById($id, 10);
        return view('logsms.index', ['title' => 'Data log sms per alat', 'buttonClearAll' => true, 'garamId' => $id, 'data' => $data]);
    }

    public function create()
    {
        return view('salt.form');
    }

    public function edit($id)
    {
        $data = Salt::where(['id' => $id])->first();
        return view('salt.form', ['data' => $data]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|min:4|max:4',
            'name' => 'required|min:0|max:150'
        ]);

        $data = [
            'code' => $request->input('code'),
            'name' => $request->input('name'),
            'users_id' => Auth::user()->id,
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Salt::insert($data);

        if ($service) 
        {
            return redirect('/salt');
        }
        else 
        {
            return redirect('/salt/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'code' => 'required|min:4|max:4',
            'name' => 'required|min:0|max:150'
        ]);

        $id = $request->input('id');

        $data = [
            'code' => $request->input('code'),
            'name' => $request->input('name'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Salt::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/salt');
        }
        else 
        {
            return redirect('/salt/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = Salt::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/salt');
        }
        else 
        {
            return redirect('/salt');
        }
    }
}