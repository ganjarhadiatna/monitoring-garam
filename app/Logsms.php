<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logsms extends Model
{
    protected $table = "logsms";

    public function scopeGetAll($quer, $limit)
    {
        return $this
        ->select(
            'salts.name',
            'salts.code',
            'logsms.id',
            'logsms.status',
            'logsms.created_at',
            'logsms.updated_at'
        )
        ->join('salts', 'salts.id', '=', 'logsms.salts_id')
        ->orderBy('logsms.id', 'desc')
        ->paginate($limit);
    }

    public function scopeGetAllWithHistory($quer)
    {
        return $this
        ->select(
            'salts.code',
            'salts.name',
            'salts.status',
            'logsms.id',
            'logsms.salts_id',
            'logsms.status'
        )
        ->join('salts', 'salts.id', '=', 'logsms.salts_id')
        ->groupBy('logsms.salts_id')
        ->orderBy('logsms.id', 'desc')
        ->get();
    }

    public function scopeGetById($quer, $id, $limit)
    {
        return $this
        ->select(
            'salts.name',
            'salts.code',
            'logsms.id',
            'logsms.status',
            'logsms.created_at',
            'logsms.updated_at'
        )
        ->where(['salts_id' => $id])
        ->join('salts', 'salts.id', '=', 'logsms.salts_id')
        ->orderBy('logsms.id', 'desc')
        ->paginate($limit);
    }
}
