<?php   
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Salt extends Model
{
    protected $table = "salts";

    // protected $fillable = [];

    // public $timestamps = false;

    public function scopeGetAll($quer, $limit)
    {
        return $this
        ->select(
            'users.name as user_name',
            'salts.id',
            'salts.code',
            'salts.name',
            'salts.status',
            'salts.created_at',
            'salts.updated_at',
            (DB::raw('(select ph from histories where salts_id=salts.id order by id desc limit 1) as ph ')),
            (DB::raw('(select tds from histories where salts_id=salts.id order by id desc limit 1) as tds ')),
            (DB::raw('(select accu from histories where salts_id=salts.id order by id desc limit 1) as accu ')),
            (DB::raw('(select panel from histories where salts_id=salts.id order by id desc limit 1) as panel '))
        )
        ->join('users', 'users.id', '=', 'salts.users_id')
        ->orderBy('salts.id', 'desc')
        ->paginate($limit);
    }

    public function scopeGetAllWithHistory($quer)
    {
        return $this
        ->select(
            'salts.id',
            'salts.code',
            'salts.name',
            'salts.status',
            (DB::raw('(select ph from histories where salts_id=salts.id order by id desc limit 1) as ph ')),
            (DB::raw('(select tds from histories where salts_id=salts.id order by id desc limit 1) as tds ')),
            (DB::raw('(select accu from histories where salts_id=salts.id order by id desc limit 1) as accu ')),
            (DB::raw('(select panel from histories where salts_id=salts.id order by id desc limit 1) as panel '))
        )
        ->orderBy('salts.id', 'desc')
        ->get();
    }
}