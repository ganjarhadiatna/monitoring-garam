<?php   
namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = "histories";

    // protected $fillable = [];

    // public $timestamps = false;

    public function scopeGetAll($quer, $limit)
    {
        return $this
        ->select(
            'salts.name',
            'salts.code',
            'histories.id',
            'histories.ph',
            'histories.tds',
            'histories.accu',
            'histories.panel',
            'histories.created_at',
            'histories.updated_at'
        )
        ->join('salts', 'salts.id', '=', 'histories.salts_id')
        ->orderBy('histories.id', 'desc')
        ->paginate($limit);
    }

    public function scopeGetAllWithHistory($quer)
    {
        return $this
        ->select(
            'salts.code',
            'salts.name',
            'salts.status',
            'histories.id',
            'histories.salts_id',
            'histories.ph',
            'histories.tds'
        )
        ->join('salts', 'salts.id', '=', 'histories.salts_id')
        ->groupBy('histories.salts_id')
        ->orderBy('histories.id', 'desc')
        ->get();
    }

    public function scopeGetById($quer, $id, $limit)
    {
        return $this
        ->select(
            'salts.name',
            'salts.code',
            'histories.id',
            'histories.ph',
            'histories.tds',
            'histories.accu',
            'histories.panel',
            'histories.created_at',
            'histories.updated_at'
        )
        ->where(['salts_id' => $id])
        ->join('salts', 'salts.id', '=', 'histories.salts_id')
        ->orderBy('histories.id', 'desc')
        ->paginate($limit);
    }
}